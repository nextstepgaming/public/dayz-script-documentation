var class_antibiotics_mdfr =
[
    [ "ActivateCondition", "d2/d2f/class_antibiotics_mdfr.html#a08b653bf6efd43e9f0217b05c1150042", null ],
    [ "DeactivateCondition", "d2/d2f/class_antibiotics_mdfr.html#a21f83c5d1b32e6d4a4de7548c19a1e4f", null ],
    [ "GetDebugText", "d2/d2f/class_antibiotics_mdfr.html#a74763042c3284bdf2c0d7f25ddb1d3ec", null ],
    [ "GetDebugTextSimple", "d2/d2f/class_antibiotics_mdfr.html#a3d27cb54a02757d35db8c30dcd4ce514", null ],
    [ "Init", "d2/d2f/class_antibiotics_mdfr.html#a28d9ca8fc9e740e9c4b32bf6d20024eb", null ],
    [ "OnActivate", "d2/d2f/class_antibiotics_mdfr.html#a384db9e6acdff41869ca3392d8f58898", null ],
    [ "OnDeactivate", "d2/d2f/class_antibiotics_mdfr.html#ab391863eed1d0b78cf3ea5616525fd11", null ],
    [ "OnReconnect", "d2/d2f/class_antibiotics_mdfr.html#ab9bb14295c11106dd23122f71c8df199", null ],
    [ "OnTick", "d2/d2f/class_antibiotics_mdfr.html#aaab90368d4c6136b5a38146008b49785", null ],
    [ "ANTIBIOTICS_LIFETIME", "d2/d2f/class_antibiotics_mdfr.html#ada43b0b775e4ff8c31560f261b3d65a8", null ],
    [ "ANTIBIOTICS_STRENGTH", "d2/d2f/class_antibiotics_mdfr.html#a53b617e945bb4e58ec1141bea089745c", null ],
    [ "m_RegenTime", "d2/d2f/class_antibiotics_mdfr.html#a6dd5b6305e1ce3744b7fa69d11326956", null ]
];