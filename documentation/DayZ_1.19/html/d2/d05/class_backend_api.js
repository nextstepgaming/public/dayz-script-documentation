var class_backend_api =
[
    [ "BackendApi", "d2/d05/class_backend_api.html#abaa4ce0dab90b50754a01d9a52917383", null ],
    [ "~BackendApi", "d2/d05/class_backend_api.html#acd4cf5ae4b13b9d8c90d342ee250c967", null ],
    [ "FeedbackMessage", "d2/d05/class_backend_api.html#a9c3f1c64b04720bc816647c6d854ba85", null ],
    [ "GetCredentialsItem", "d2/d05/class_backend_api.html#a0039eb521574abf9d4c21f83ae607969", null ],
    [ "GetErrorCode", "d2/d05/class_backend_api.html#afa7561dd8d56b70ed9f7e47efd33c975", null ],
    [ "Initiate", "d2/d05/class_backend_api.html#a58124d2211ea7e00a72daf0c1e93d6ca", null ],
    [ "IsBusy", "d2/d05/class_backend_api.html#ab09d777687a418dea555a621e838fdfb", null ],
    [ "IsDisconnected", "d2/d05/class_backend_api.html#a32ab295d4bc08fc2bad5d3ecf71edce1", null ],
    [ "IsRuntime", "d2/d05/class_backend_api.html#af831afcb32d4ab37daea3f94c1534412", null ],
    [ "OnCannotInitiate", "d2/d05/class_backend_api.html#a0c7f27af203f0baf146720bae1ca6822", null ],
    [ "OnCannotShutdown", "d2/d05/class_backend_api.html#a70de65fb75e71b1e41e0095f863d0ded", null ],
    [ "OnFail", "d2/d05/class_backend_api.html#ab94114ac8d8f5532e2af52157454085d", null ],
    [ "OnSuccess", "d2/d05/class_backend_api.html#a1f6c74105d00abe07c42453f29799619", null ],
    [ "PlayerRequest", "d2/d05/class_backend_api.html#a07e683a2175b2b247f6871d9bda4d67a", null ],
    [ "Request", "d2/d05/class_backend_api.html#a6cc233b146cdf7df89da12e6a6ee61ae", null ],
    [ "SetCredentialsItem", "d2/d05/class_backend_api.html#aba896f432f23bc4d14579d04e2ea99b4", null ],
    [ "Shutdown", "d2/d05/class_backend_api.html#a88fd1e3fe24f846c6d18dd1d724d9129", null ],
    [ "VerifyCredentials", "d2/d05/class_backend_api.html#a30d2cfccdf01354c9deb71d28126a10a", null ]
];