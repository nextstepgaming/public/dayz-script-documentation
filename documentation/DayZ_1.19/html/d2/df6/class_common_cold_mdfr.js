var class_common_cold_mdfr =
[
    [ "ActivateCondition", "d2/df6/class_common_cold_mdfr.html#ae10504b8dba5f6384c1381e82ea0ce1d", null ],
    [ "DeactivateCondition", "d2/df6/class_common_cold_mdfr.html#a3e05e8bcb304c25fb724aeab43ae8eda", null ],
    [ "GetDebugText", "d2/df6/class_common_cold_mdfr.html#a3fee56dcd995c129ca708ef5d730ecd9", null ],
    [ "Init", "d2/df6/class_common_cold_mdfr.html#a4265c398e1f776f1c309ece6e53851f2", null ],
    [ "OnActivate", "d2/df6/class_common_cold_mdfr.html#a193c4d08a72d8dba0926780fb8ee7c4a", null ],
    [ "OnDeactivate", "d2/df6/class_common_cold_mdfr.html#a8731905c26c7a2fdb986d0558d597172", null ],
    [ "OnTick", "d2/df6/class_common_cold_mdfr.html#aa714e58474c76a2e4488b800b56eccc3", null ],
    [ "AGENT_THRESHOLD_ACTIVATE", "d2/df6/class_common_cold_mdfr.html#ac86617d1936966b2470cf3da3e7f3be1", null ],
    [ "AGENT_THRESHOLD_DEACTIVATE", "d2/df6/class_common_cold_mdfr.html#a28fb7b52a2d0381337c58e5e274d052e", null ]
];