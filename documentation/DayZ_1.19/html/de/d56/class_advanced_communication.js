var class_advanced_communication =
[
    [ "EnableBroadcast", "de/d56/class_advanced_communication.html#ac643f224738b2289ac7f7e4965bcf994", null ],
    [ "EnableReceive", "de/d56/class_advanced_communication.html#ab04aee3c6f4332460b50fdddf17d6ec6", null ],
    [ "GetTunedFrequency", "de/d56/class_advanced_communication.html#ab8810aa5ea89291d6a2618fa3ff58ea0", null ],
    [ "GetTunedFrequencyIndex", "de/d56/class_advanced_communication.html#ae57d663978b07cdbbf8492e216cbe660", null ],
    [ "IsBroadcasting", "de/d56/class_advanced_communication.html#ab688ebaeca4fe96553f9337511191bb6", null ],
    [ "IsOn", "de/d56/class_advanced_communication.html#ac57414fb4b31a899b5e713d20963dd23", null ],
    [ "IsOn", "de/d56/class_advanced_communication.html#ac57414fb4b31a899b5e713d20963dd23", null ],
    [ "IsReceiving", "de/d56/class_advanced_communication.html#aa87292af3589213d0a144eaa6d965f2b", null ],
    [ "SetFrequencyByIndex", "de/d56/class_advanced_communication.html#a49fefa720f90b4b287e998f09b68e4c4", null ],
    [ "SetNextChannel", "de/d56/class_advanced_communication.html#af2ab9b44767e1e1de235ac805b791cf9", null ],
    [ "SetPrevChannel", "de/d56/class_advanced_communication.html#a72b7adff29065585c5d5b73b8bd7c32c", null ],
    [ "SwitchOn", "de/d56/class_advanced_communication.html#a3394deabd221214ae1ba437f0c772f86", null ],
    [ "SwitchOn", "de/d56/class_advanced_communication.html#a3394deabd221214ae1ba437f0c772f86", null ]
];