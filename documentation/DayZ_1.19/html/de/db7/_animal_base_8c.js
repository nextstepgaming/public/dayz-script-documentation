var _animal_base_8c =
[
    [ "DayZAnimal", "d1/dcc/class_day_z_animal.html", "d1/dcc/class_day_z_animal" ],
    [ "Animal_BosTaurus", "de/db7/_animal_base_8c.html#d2/d59/class_animal___bos_taurus", null ],
    [ "AnimalBase", "d4/d8f/class_animal_base.html", "d4/d8f/class_animal_base" ],
    [ "Animal_CapreolusCapreolus", "de/db7/_animal_base_8c.html#d1/d6d/class_animal___capreolus_capreolus", null ],
    [ "Animal_CervusElaphus", "de/db7/_animal_base_8c.html#dc/d62/class_animal___cervus_elaphus", null ],
    [ "Animal_GallusGallusDomesticus", "d5/d8c/class_animal___gallus_gallus_domesticus.html", "d5/d8c/class_animal___gallus_gallus_domesticus" ],
    [ "Animal_GallusGallusDomesticusF", "da/d9d/class_animal___gallus_gallus_domesticus_f.html", "da/d9d/class_animal___gallus_gallus_domesticus_f" ],
    [ "CanBeSkinned", "de/db7/_animal_base_8c.html#ac41954dab14abd446b1c1ad9b3beab0e", null ],
    [ "CaptureSound", "de/db7/_animal_base_8c.html#a9963015efd45ba5607f7ac42e039f4d7", null ],
    [ "GetDeadItemName", "de/db7/_animal_base_8c.html#a839948b6f7b99a323f4335d035a93e94", null ],
    [ "IsDanger", "de/db7/_animal_base_8c.html#aa66f6a10a167b3aab1ced9f813a84873", null ],
    [ "IsRefresherSignalingViable", "de/db7/_animal_base_8c.html#ae18bf202ea7e79b44ee3d5a9f7a42703", null ],
    [ "KeepHealthOnReplace", "de/db7/_animal_base_8c.html#ae5ccab0782b5c43c9bef337d3216c8da", null ],
    [ "RegisterHitComponentsForAI", "de/db7/_animal_base_8c.html#ae52be22902445a9e766534119decaf6d", null ],
    [ "ReleaseSound", "de/db7/_animal_base_8c.html#a7610f1938e082304c0a182cec4882c85", null ],
    [ "ReplaceOnDeath", "de/db7/_animal_base_8c.html#a57f78023773e268aced849838649291a", null ]
];