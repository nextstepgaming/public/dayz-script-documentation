var class_bios_package_service =
[
    [ "CheckUpdateAsync", "df/dcf/class_bios_package_service.html#a815783b84181ed288e99a4d56df4a747", null ],
    [ "OnCheckUpdate", "df/dcf/class_bios_package_service.html#a5b8d097cbd03264e31c27f4e9b2fd16d", null ],
    [ "OnPromptUpdate", "df/dcf/class_bios_package_service.html#a49cc851fc4c77a12dc08ea6df2eee44b", null ],
    [ "OnShowStore", "df/dcf/class_bios_package_service.html#a1f605b6ab91d88be550bff5e23c8d52b", null ],
    [ "PromptUpdateAsync", "df/dcf/class_bios_package_service.html#af8ff1b0b46a9d12dd77cb6d550806991", null ],
    [ "ShowStoreAsync", "df/dcf/class_bios_package_service.html#a8eb99267203a3a9c2fafea570ccc8644", null ]
];