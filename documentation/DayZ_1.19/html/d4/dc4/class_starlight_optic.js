var class_starlight_optic =
[
    [ "GetCurrentNVType", "d4/dc4/class_starlight_optic.html#a1c8e0ab70fe8176e472a225f30d2dc7a", null ],
    [ "InitOpticMode", "d4/dc4/class_starlight_optic.html#a19d434abd7095f9afc540159efce628a", null ],
    [ "OnOpticEnter", "d4/dc4/class_starlight_optic.html#a47d065c3953572670514631bf62cf447", null ],
    [ "OnOpticModeChange", "d4/dc4/class_starlight_optic.html#aef8ab73a183caf3c13f43671d1e68490", null ],
    [ "SetActions", "d4/dc4/class_starlight_optic.html#a635adf5be16d2e895dd2e88c552298ad", null ],
    [ "UpdateSelectionVisibility", "d4/dc4/class_starlight_optic.html#a878dbaee358685814e402ca3c1972f36", null ]
];