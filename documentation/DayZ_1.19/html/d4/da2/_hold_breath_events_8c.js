var _hold_breath_events_8c =
[
    [ "PlayerSoundEventBase", "d5/def/class_player_sound_event_base.html", "d5/def/class_player_sound_event_base" ],
    [ "HoldBreathSoundEventBase", "d2/deb/class_hold_breath_sound_event_base.html", "d2/deb/class_hold_breath_sound_event_base" ],
    [ "ExhaustedBreathSoundEvent", "d4/da2/_hold_breath_events_8c.html#a65d5377532cd69592ad812944428dc49", null ],
    [ "HasHoldBreathException", "d4/da2/_hold_breath_events_8c.html#a154c86f8de1a5cff3a8347d3d21ddb95", null ],
    [ "HasPriorityOverCurrent", "d4/da2/_hold_breath_events_8c.html#a286474a6b59c193baa69ae5266d4259b", null ],
    [ "HoldBreathSoundEvent", "d4/da2/_hold_breath_events_8c.html#a7d0af8b9d1282f50184ff48a58bc74df", null ],
    [ "HoldBreathSoundEventBase", "d4/da2/_hold_breath_events_8c.html#a443ff1bfa7bfcb1e08beefe3d514200f", null ],
    [ "ReleaseBreathSoundEvent", "d4/da2/_hold_breath_events_8c.html#a4682277b485fdcb91b9de7713d7d650d", null ]
];