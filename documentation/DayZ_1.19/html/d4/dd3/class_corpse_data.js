var class_corpse_data =
[
    [ "CorpseData", "d4/dd3/class_corpse_data.html#a403064a247bbb22344eeb2c265efa3af", null ],
    [ "UpdateCorpseState", "d4/dd3/class_corpse_data.html#aa825ba536ce7a3eb8c8db6692325e540", null ],
    [ "GET_LIFETIME_TRIES_MAX", "d4/dd3/class_corpse_data.html#a74d292e8009313200e163102feb0d854", null ],
    [ "m_bUpdate", "d4/dd3/class_corpse_data.html#a9ab0dbb6b50c3451ad0f896230690da3", null ],
    [ "m_iCorpseState", "d4/dd3/class_corpse_data.html#ada5959c60609132f05beb1175e04c30c", null ],
    [ "m_iLastUpdateTime", "d4/dd3/class_corpse_data.html#a8342f6c5854a42f1ad8e71af54210d36", null ],
    [ "m_iMaxLifetime", "d4/dd3/class_corpse_data.html#a0111eb7e80a17ffa168f763d6768e27b", null ],
    [ "m_iTimeOfDeath", "d4/dd3/class_corpse_data.html#a5ed6c4607333c6bf53b8caebaa309c91", null ],
    [ "m_iTriesToGetLifetime", "d4/dd3/class_corpse_data.html#a0081800fc1a33f91e79d8e99b1d84766", null ],
    [ "m_Player", "d4/dd3/class_corpse_data.html#a6d6135ae56b3c1f3d1a647b90f1a5c53", null ]
];