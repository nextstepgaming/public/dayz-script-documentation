var class_weather_phenomenon =
[
    [ "WeatherPhenomenon", "d8/d95/class_weather_phenomenon.html#aadb3db3ac70ee7bf9253400798bead50", null ],
    [ "~WeatherPhenomenon", "d8/d95/class_weather_phenomenon.html#a5fbc174c371b665b36729fc4d8ea9ac6", null ],
    [ "GetActual", "d8/d95/class_weather_phenomenon.html#a4de4ba7dc392b83d8d392843c558db0b", null ],
    [ "GetForecast", "d8/d95/class_weather_phenomenon.html#ade4a5372108a3439bdc4d06f86cd946c", null ],
    [ "GetForecastChangeLimits", "d8/d95/class_weather_phenomenon.html#aa03167ac766760a6da35ae490c16fe1b", null ],
    [ "GetForecastTimeLimits", "d8/d95/class_weather_phenomenon.html#a7b635fd9d0ee746943bbc896d0f3e397", null ],
    [ "GetLimits", "d8/d95/class_weather_phenomenon.html#a051c23ce346ad0939fb0cbd17643629d", null ],
    [ "GetNextChange", "d8/d95/class_weather_phenomenon.html#a9925d0c45600c13cb0275415b5b72e44", null ],
    [ "GetType", "d8/d95/class_weather_phenomenon.html#a91dd274bd0164b058277677d0be67908", null ],
    [ "OnBeforeChange", "d8/d95/class_weather_phenomenon.html#a60422c10b92c859d91f40536461fe76b", null ],
    [ "Set", "d8/d95/class_weather_phenomenon.html#a19c052f7b682ceb45b65999153254ea8", null ],
    [ "SetForecastChangeLimits", "d8/d95/class_weather_phenomenon.html#a5ae97bfc99aaba2be64ad8b54154df7f", null ],
    [ "SetForecastTimeLimits", "d8/d95/class_weather_phenomenon.html#af1faa836e3a3a135804a297b605c6708", null ],
    [ "SetLimits", "d8/d95/class_weather_phenomenon.html#a57d3e97e1f847fb84aefd8219ec9744f", null ],
    [ "SetNextChange", "d8/d95/class_weather_phenomenon.html#a5543d07067b7a14ebe6fd85c4f34840d", null ]
];