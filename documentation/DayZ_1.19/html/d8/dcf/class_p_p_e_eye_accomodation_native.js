var class_p_p_e_eye_accomodation_native =
[
    [ "GetPostProcessEffectID", "d8/dcf/class_p_p_e_eye_accomodation_native.html#a5854c50a34b7487bbefd8d409110a1ba", null ],
    [ "RegisterMaterialParameters", "d8/dcf/class_p_p_e_eye_accomodation_native.html#afa42aa1a5477d1e4b11006ef4d8d6961", null ],
    [ "SetFinalParameterValue", "d8/dcf/class_p_p_e_eye_accomodation_native.html#a3de67d8ab0945acefaed48012b1928e7", null ],
    [ "L_0_BURLAP", "d8/dcf/class_p_p_e_eye_accomodation_native.html#ad9c47dea8cef15c0c14ddadf8dc5fe44", null ],
    [ "L_0_NVG_GENERIC", "d8/dcf/class_p_p_e_eye_accomodation_native.html#ac1f3b26c1dc718e3bc2f2b7af3c641a8", null ],
    [ "L_0_UNDERGROUND", "d8/dcf/class_p_p_e_eye_accomodation_native.html#ae08531a4103690fceff5d940346369ec", null ],
    [ "PARAM_INTENSITY", "d8/dcf/class_p_p_e_eye_accomodation_native.html#ae87387f0b156bfd3adebe7029824db99", null ]
];