var class_potato =
[
    [ "CanBeCooked", "d8/d39/class_potato.html#a9d0c033c1e1c7f16e98e40392690c303", null ],
    [ "CanBeCookedOnStick", "d8/d39/class_potato.html#abe60f8df3ea44d0371e98f721702d2d6", null ],
    [ "CanDecay", "d8/d39/class_potato.html#ae3d4af797365331927d12c91752a22d4", null ],
    [ "EEOnCECreate", "d8/d39/class_potato.html#a166c3a754c1f90680857ad3e1393647f", null ],
    [ "IsFruit", "d8/d39/class_potato.html#a5e3bdf507581e83bfccb737468acb4de", null ],
    [ "SetActions", "d8/d39/class_potato.html#adc4caa7de0716785f4d5cc41963d8833", null ]
];