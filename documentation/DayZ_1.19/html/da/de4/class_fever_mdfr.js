var class_fever_mdfr =
[
    [ "ActivateCondition", "da/de4/class_fever_mdfr.html#a18cd308db81bcf0e9d59568f0bfea3dd", null ],
    [ "DeactivateCondition", "da/de4/class_fever_mdfr.html#a6939e2fc110650cec325cd78a02b5b81", null ],
    [ "Init", "da/de4/class_fever_mdfr.html#acfc4e3381770a18cccf031466cb17054", null ],
    [ "OnActivate", "da/de4/class_fever_mdfr.html#a04a2737a8c13199d3452310be9690419", null ],
    [ "OnDeactivate", "da/de4/class_fever_mdfr.html#a5c3bb7857556f3f7ab4ce68da67e699b", null ],
    [ "OnReconnect", "da/de4/class_fever_mdfr.html#a0f8d16b142141d037ac1d00bfa1ab7d2", null ],
    [ "OnTick", "da/de4/class_fever_mdfr.html#af44efe8505863f8075cfc985595edc7b", null ],
    [ "EVENT_INTERVAL_MAX", "da/de4/class_fever_mdfr.html#abecb25e97a374db616ccd6b831a167c7", null ],
    [ "EVENT_INTERVAL_MIN", "da/de4/class_fever_mdfr.html#abb420c328239fa9088ec4ab7f4c75b36", null ],
    [ "m_NextEvent", "da/de4/class_fever_mdfr.html#ac246800706ec7bf65213495e3a10cc7b", null ],
    [ "m_Time", "da/de4/class_fever_mdfr.html#a8c53f6a7254e03d8b19fcdb468566c8a", null ]
];