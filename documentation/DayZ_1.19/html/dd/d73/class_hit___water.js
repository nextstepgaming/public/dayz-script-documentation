var class_hit___water =
[
    [ "Hit_Water", "dd/d73/class_hit___water.html#a7d5738d271dbb7ba1e0dd4dd7ef9344c", null ],
    [ "CalculateStoppingForce", "dd/d73/class_hit___water.html#a4cf5d4f2b324b3eae330bdc10bb27684", null ],
    [ "EvaluateEffect", "dd/d73/class_hit___water.html#a410673ec8008178b9c335764663fb16d", null ],
    [ "OnEnterAngledCalculations", "dd/d73/class_hit___water.html#aa59e96611cac1d1fb24b677714b4ace0", null ],
    [ "OnEnterCalculations", "dd/d73/class_hit___water.html#aa1461a6f49aecf5478e315a6947b262b", null ],
    [ "OnExitCalculations", "dd/d73/class_hit___water.html#aab277fc0c51333869310777880da1598", null ],
    [ "OnRicochetCalculations", "dd/d73/class_hit___water.html#a0028456f0236c83971af31ebdbee7064", null ]
];