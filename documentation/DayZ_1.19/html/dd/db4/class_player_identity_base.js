var class_player_identity_base =
[
    [ "PlayerIdentityBase", "dd/db4/class_player_identity_base.html#a7b3f7c374e30f9e0711e0cbffe389287", null ],
    [ "~PlayerIdentityBase", "dd/db4/class_player_identity_base.html#af853261640236fd69dbe1418b229d9f6", null ],
    [ "GetBandwidthAvg", "dd/db4/class_player_identity_base.html#a6e10de71c5e29745445cdde50069b306", null ],
    [ "GetBandwidthMax", "dd/db4/class_player_identity_base.html#a9dff2685974733c836b07c9091e5c795", null ],
    [ "GetBandwidthMin", "dd/db4/class_player_identity_base.html#a5ffecd38750b35aef9232d51a2d59404", null ],
    [ "GetFullName", "dd/db4/class_player_identity_base.html#a77adfc0e0c3262791f1d79a5e5fa647d", null ],
    [ "GetId", "dd/db4/class_player_identity_base.html#ac2e96d43abaedd547b4dfc8f9c50ed4f", null ],
    [ "GetName", "dd/db4/class_player_identity_base.html#a9200ce6bf54877be6f2c98524dc016db", null ],
    [ "GetPingAvg", "dd/db4/class_player_identity_base.html#a6b525cfb99aaa5dd8989f2b3bca8db6f", null ],
    [ "GetPingMax", "dd/db4/class_player_identity_base.html#a7a7e013a494fd5c168a6bb90d3403efd", null ],
    [ "GetPingMin", "dd/db4/class_player_identity_base.html#a49087f010d718ce8e0dd122419f47e0e", null ],
    [ "GetPlainId", "dd/db4/class_player_identity_base.html#a22e380e2badedc7f321be92f2e66fdbb", null ],
    [ "GetPlayer", "dd/db4/class_player_identity_base.html#a58cd12503266f8c861cd78ecc1d1a245", null ],
    [ "GetPlayerId", "dd/db4/class_player_identity_base.html#a49a24646c6600a399258bf200a37d7b7", null ]
];