var dir_2f487fd4525d42d8067e7d1b4828f2de =
[
    [ "conditions", "dir_af5a0bbd16f1deac0f80732119cbe418.html", "dir_af5a0bbd16f1deac0f80732119cbe418" ],
    [ "diseases", "dir_ca7d4b78184f31e916245373efccd52e.html", "dir_ca7d4b78184f31e916245373efccd52e" ],
    [ "Antibiotics.c", "d0/d52/_antibiotics_8c.html", "d0/d52/_antibiotics_8c" ],
    [ "BloodRegen.c", "da/da9/_blood_regen_8c.html", "da/da9/_blood_regen_8c" ],
    [ "BoneRegen.c", "d5/d64/_bone_regen_8c.html", "d5/d64/_bone_regen_8c" ],
    [ "BreathVapourMdfr.c", "db/d04/_breath_vapour_mdfr_8c.html", "db/d04/_breath_vapour_mdfr_8c" ],
    [ "CharcoalMdfr.c", "db/d99/_charcoal_mdfr_8c.html", "db/d99/_charcoal_mdfr_8c" ],
    [ "DisinfectMdfr.c", "d7/d6c/_disinfect_mdfr_8c.html", "d7/d6c/_disinfect_mdfr_8c" ],
    [ "Drowning.c", "d0/d89/_drowning_8c.html", "d0/d89/_drowning_8c" ],
    [ "EpinephrineMdfr.c", "d0/d42/_epinephrine_mdfr_8c.html", "d0/d42/_epinephrine_mdfr_8c" ],
    [ "Flies.c", "d3/d55/_flies_8c.html", "d3/d55/_flies_8c" ],
    [ "Health.c", "d0/df8/_health_8c.html", "d0/df8/_health_8c" ],
    [ "HealthRegen.c", "dd/da6/_health_regen_8c.html", "dd/da6/_health_regen_8c" ],
    [ "HeatComfortMdfr.c", "d2/d61/_heat_comfort_mdfr_8c.html", "d2/d61/_heat_comfort_mdfr_8c" ],
    [ "Hunger.c", "d0/d99/_hunger_8c.html", "d0/d99/_hunger_8c" ],
    [ "ImmuneSystem.c", "d8/d3b/_immune_system_8c.html", "d8/d3b/_immune_system_8c" ],
    [ "ImmunityBoost.c", "d3/df6/_immunity_boost_8c.html", "d3/df6/_immunity_boost_8c" ],
    [ "Mask.c", "d6/d38/_mask_8c.html", "d6/d38/_mask_8c" ],
    [ "MorphineMdfr.c", "db/dc6/_morphine_mdfr_8c.html", "db/dc6/_morphine_mdfr_8c" ],
    [ "PainKillersMdfr.c", "df/d9d/_pain_killers_mdfr_8c.html", "df/d9d/_pain_killers_mdfr_8c" ],
    [ "Saline.c", "dc/d41/_saline_8c.html", "dc/d41/_saline_8c" ],
    [ "Shock.c", "d2/d7f/_shock_8c.html", "d2/d7f/_shock_8c" ],
    [ "ShockDamage.c", "de/d4f/_shock_damage_8c.html", "de/d4f/_shock_damage_8c" ],
    [ "Stomach.c", "d3/d5a/_stomach_8c.html", "d3/d5a/_stomach_8c" ],
    [ "Testing.c", "d2/d24/_testing_8c.html", "d2/d24/_testing_8c" ],
    [ "Thirst.c", "d0/d30/_thirst_8c.html", "d0/d30/_thirst_8c" ],
    [ "Toxicity.c", "d7/da9/_toxicity_8c.html", "d7/da9/_toxicity_8c" ],
    [ "Unconsciousness.c", "d0/d98/_unconsciousness_8c.html", "d0/d98/_unconsciousness_8c" ]
];