var _symptom_events_8c =
[
    [ "PlayerSoundEventBase", "d5/def/class_player_sound_event_base.html", "d5/def/class_player_sound_event_base" ],
    [ "SymptomSoundEventBase", "d7/da6/class_symptom_sound_event_base.html", "d7/da6/class_symptom_sound_event_base" ],
    [ "CoughSoundEvent", "d5/dcb/_symptom_events_8c.html#a207896621c17c9d637c10b5e11f5c85f", null ],
    [ "HasPriorityOverCurrent", "d5/dcb/_symptom_events_8c.html#a70d57cdfdaa2e33fa903f5bfe9852d33", null ],
    [ "LaugherSoundEvent", "d5/dcb/_symptom_events_8c.html#a6e45ca7de2198151d5f0d5257bd147da", null ],
    [ "SneezeSoundEvent", "d5/dcb/_symptom_events_8c.html#ad884b1b45617e0ec9827db559069bf57", null ],
    [ "SymptomSoundEventBase", "d5/dcb/_symptom_events_8c.html#a76c58bf220a11d2b46157cc02e4fd36e", null ]
];