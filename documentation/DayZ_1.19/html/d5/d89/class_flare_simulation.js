var class_flare_simulation =
[
    [ "FlareSimulation", "d5/d89/class_flare_simulation.html#a9bc7274330243f06a1709ca512a63201", null ],
    [ "~FlareSimulation", "d5/d89/class_flare_simulation.html#a26f91f337498f42b223d8ab888b4c474", null ],
    [ "CastFlareAINoise", "d5/d89/class_flare_simulation.html#a5095f94bf17920b352ecb6e8794dd1dd", null ],
    [ "OnActivation", "d5/d89/class_flare_simulation.html#a9c0cc0032b46473535a318b667607ffa", null ],
    [ "OnFire", "d5/d89/class_flare_simulation.html#ab3d5fdb6a9ffa2e6bede12fae510aac6", null ],
    [ "OnTermination", "d5/d89/class_flare_simulation.html#a92f74660c99a703c64f9936e443ae11b", null ],
    [ "Simulate", "d5/d89/class_flare_simulation.html#ae91feea70480d21a6c7550526f0b428a", null ],
    [ "TurnOffDistantLight", "d5/d89/class_flare_simulation.html#a5bc47a50e0a78fd546ab9f7e8c8ad35c", null ],
    [ "m_BurningSound", "d5/d89/class_flare_simulation.html#a94d7253c9526eda5f2ae999ac5ad9710", null ],
    [ "m_FlareLight", "d5/d89/class_flare_simulation.html#a5162e86f780da430269126db101b2c33", null ],
    [ "m_LastNoiseTime", "d5/d89/class_flare_simulation.html#a801ad4a33f2b19ba9b6d21b68f011495", null ],
    [ "m_NoisePar", "d5/d89/class_flare_simulation.html#a765110f996503690a55a6e8c51c372e5", null ],
    [ "m_NoiseTimer", "d5/d89/class_flare_simulation.html#a05942629b2e3d3eeb044db8d32aa13b1", null ],
    [ "m_ParMainFire", "d5/d89/class_flare_simulation.html#ab2730e538de80335129afaf2cff1870f", null ],
    [ "m_ParticleId", "d5/d89/class_flare_simulation.html#abc5769c8bbd1a3bb11c7f8f6ee2aa98e", null ],
    [ "m_ScriptedLight", "d5/d89/class_flare_simulation.html#af30b3096d87200887272144ec120f3ed", null ],
    [ "MAX_FARLIGHT_DIST", "d5/d89/class_flare_simulation.html#a4cac772619c275c63b3700d9ee1f0ba9", null ],
    [ "MIN_FARLIGHT_DIST", "d5/d89/class_flare_simulation.html#ab376a8f4cdf7a0fb28a9cef37992737b", null ],
    [ "NOISE_DELAY", "d5/d89/class_flare_simulation.html#ae488f187840b9f774b9d3143cd980979", null ]
];