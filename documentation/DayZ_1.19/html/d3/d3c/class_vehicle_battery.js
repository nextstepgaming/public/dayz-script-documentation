var class_vehicle_battery =
[
    [ "VehicleBattery", "d3/d3c/class_vehicle_battery.html#a029dac5ea1ee50d7d5d781573533cc40", null ],
    [ "CanDetachAttachment", "d3/d3c/class_vehicle_battery.html#a8f30cad3d2b47f9da1e0f96deaa7426c", null ],
    [ "CanDisplayAttachmentSlot", "d3/d3c/class_vehicle_battery.html#a03529f764e85fe4153336418b6a2e426", null ],
    [ "CanPutAsAttachment", "d3/d3c/class_vehicle_battery.html#a1b4fb28895d01b2c70c0617b2bf9c2cb", null ],
    [ "CanPutInCargo", "d3/d3c/class_vehicle_battery.html#ae06c239f22dc582e8ac2e861f6b95c4f", null ],
    [ "CanPutIntoHands", "d3/d3c/class_vehicle_battery.html#ad746e8983b6d1af95ab721a493f66d26", null ],
    [ "CanReceiveAttachment", "d3/d3c/class_vehicle_battery.html#ad59c82e4fa2138a19f29d26f6a5764a4", null ],
    [ "DisplayNameRuinAttach", "d3/d3c/class_vehicle_battery.html#ab7f611c37afa639f555509d753629559", null ],
    [ "GetEfficiency0To1", "d3/d3c/class_vehicle_battery.html#acadf5501d57d06baf19d1ff60650a989", null ],
    [ "GetEfficiencyDecayStart", "d3/d3c/class_vehicle_battery.html#a5da425a155a09c3863e47676c05f8aa3", null ],
    [ "OnEnergyAdded", "d3/d3c/class_vehicle_battery.html#a077927f9b87baba6e4a27e09e1591805", null ],
    [ "OnEnergyConsumed", "d3/d3c/class_vehicle_battery.html#a339786d36818978f72c77e7d8737ff91", null ],
    [ "OnInventoryEnter", "d3/d3c/class_vehicle_battery.html#a4fcef66968771656ccc51a842c6043b0", null ],
    [ "OnMovedInsideCargo", "d3/d3c/class_vehicle_battery.html#a86f1410cea01605b8314d09e82147024", null ],
    [ "OnQuantityChanged", "d3/d3c/class_vehicle_battery.html#a48e607edd98ca37b596424d453999b65", null ],
    [ "SetActions", "d3/d3c/class_vehicle_battery.html#a182115b173784e0e940e8202c78d188e", null ],
    [ "SetCEBasedQuantity", "d3/d3c/class_vehicle_battery.html#a1b1a91956e7dd8c1257a1f5f96e23fa2", null ],
    [ "ShowZonesHealth", "d3/d3c/class_vehicle_battery.html#afba9833d8cfe497d25629e51506f6659", null ],
    [ "m_Efficiency0To10", "d3/d3c/class_vehicle_battery.html#a7c4bd558b7011a60fc011e93903643a9", null ],
    [ "m_EfficiencyDecayStart", "d3/d3c/class_vehicle_battery.html#aac834f573a1cff3144ef0e0e57814e95", null ]
];