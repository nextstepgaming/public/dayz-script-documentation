var class_hit_info =
[
    [ "GetAmmoType", "d3/d34/class_hit_info.html#a5636ca183b2d1a1ac1a203d7c127918e", null ],
    [ "GetPosition", "d3/d34/class_hit_info.html#a27304bc279a1e7e59edac08c09482aa2", null ],
    [ "GetSurface", "d3/d34/class_hit_info.html#ac5303f24fb2399852dc0c15186f7fb3a", null ],
    [ "GetSurfaceNoiseMultiplier", "d3/d34/class_hit_info.html#afad95fc7f6d59c034847e3eb17bf2af3", null ],
    [ "GetSurfaceNormal", "d3/d34/class_hit_info.html#acd06a1622e13e28ce34c30da47124a7f", null ],
    [ "IsWater", "d3/d34/class_hit_info.html#a83464139886f7edb65484987a3b0734d", null ]
];