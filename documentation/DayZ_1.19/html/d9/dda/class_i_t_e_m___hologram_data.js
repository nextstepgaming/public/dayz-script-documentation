var class_i_t_e_m___hologram_data =
[
    [ "InitServer", "d9/dda/class_i_t_e_m___hologram_data.html#a228f7618b45d90f0692d27cbee3d22a6", null ],
    [ "disableHeightPlacementCheck", "d9/dda/class_i_t_e_m___hologram_data.html#a4cb58ec522fb90f8ebe34515e91f2d98", null ],
    [ "disableIsBaseViableCheck", "d9/dda/class_i_t_e_m___hologram_data.html#a06496fa9d9630fdcdcde4be1ea9b96c5", null ],
    [ "disableIsClippingRoofCheck", "d9/dda/class_i_t_e_m___hologram_data.html#ab2ce4af4f0e5eec967356bf503382791", null ],
    [ "disableIsCollidingAngleCheck", "d9/dda/class_i_t_e_m___hologram_data.html#a830f98f777cbfddba01b57ae730a578e", null ],
    [ "disableIsCollidingBBoxCheck", "d9/dda/class_i_t_e_m___hologram_data.html#a25a377fd5027a38966e216d0f02ab179", null ],
    [ "disableIsCollidingGPlotCheck", "d9/dda/class_i_t_e_m___hologram_data.html#ac49130f16491dba8a0df06f9d551c39e", null ],
    [ "disableIsCollidingPlayerCheck", "d9/dda/class_i_t_e_m___hologram_data.html#a56e9095454375aaffec48b97b2c7930e", null ],
    [ "disableIsInTerrainCheck", "d9/dda/class_i_t_e_m___hologram_data.html#abe47119cc48ba2930ffcaeea9654b0e4", null ],
    [ "disableIsPlacementPermittedCheck", "d9/dda/class_i_t_e_m___hologram_data.html#ad4e4183997c7b2989ec48c972a4864bb", null ],
    [ "disableIsUnderwaterCheck", "d9/dda/class_i_t_e_m___hologram_data.html#a76192571bd60ca48f4d9c0362a873eab", null ]
];