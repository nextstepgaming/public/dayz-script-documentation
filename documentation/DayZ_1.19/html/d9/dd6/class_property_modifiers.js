var class_property_modifiers =
[
    [ "PropertyModifiers", "d9/dd6/class_property_modifiers.html#a5c09a8f73a33695cdcaa10b70b2ad757", null ],
    [ "CalculateBarrelLength", "d9/dd6/class_property_modifiers.html#a1de0ccb0fc31e5528a0aab22aa49fa36", null ],
    [ "GetBarrelLength", "d9/dd6/class_property_modifiers.html#a936e1131d90e5762496c19bdba34e980", null ],
    [ "GetModifierRaw", "d9/dd6/class_property_modifiers.html#a71d49527e61be76e53706262f898202b", null ],
    [ "UpdateModifiers", "d9/dd6/class_property_modifiers.html#a4bc8ad9f70e2c5c7007a255616d110d2", null ],
    [ "m_BarrelLength", "d9/dd6/class_property_modifiers.html#a323e1b28cb3f85f826cfeb0d7ca36ea2", null ],
    [ "m_OwnerItem", "d9/dd6/class_property_modifiers.html#ad2ea87e8251528178bcd9a581b6d3b6e", null ],
    [ "m_RecoilModifiers", "d9/dd6/class_property_modifiers.html#aa7a8ab9c949870eeaaa0fc32963ec70c", null ],
    [ "m_SightMisalignment", "d9/dd6/class_property_modifiers.html#a7681c9a1a85bbdc029bf010f1c5edf04", null ],
    [ "m_SwayModifiers", "d9/dd6/class_property_modifiers.html#abe058682719c29ddb22fc3a34b84447a", null ]
];