var class_c_a_continuous_time =
[
    [ "CAContinuousTime", "d1/dc2/class_c_a_continuous_time.html#aac10897ca1171d520283df7e84a2a145", null ],
    [ "Cancel", "d1/dc2/class_c_a_continuous_time.html#abd5f53185613a50bf4ccfa9c40544990", null ],
    [ "Execute", "d1/dc2/class_c_a_continuous_time.html#a17c8396f939f829bbb837c9bb302e1d1", null ],
    [ "GetProgress", "d1/dc2/class_c_a_continuous_time.html#ab6b9b8490e52990ca30678c90ba2faf5", null ],
    [ "Setup", "d1/dc2/class_c_a_continuous_time.html#aa28a871858ef4b7ad44f91dcd233688e", null ],
    [ "m_AdjustedTimeToComplete", "d1/dc2/class_c_a_continuous_time.html#ad390c8f075539c9d788b0dc9156960ed", null ],
    [ "m_DefaultTimeToComplete", "d1/dc2/class_c_a_continuous_time.html#a8ef1c41d4ddc6d18b4ecc6a8156b51d0", null ],
    [ "m_LocalTimeElpased", "d1/dc2/class_c_a_continuous_time.html#aad039f2512719cb6c154e2d1e4cffd4c", null ],
    [ "m_SpentUnits", "d1/dc2/class_c_a_continuous_time.html#a03bc631cde6af808f7c2609ef7c96b7a", null ],
    [ "m_TimeElpased", "d1/dc2/class_c_a_continuous_time.html#ad5e588fa9095931134ab1fa95d766c47", null ]
];