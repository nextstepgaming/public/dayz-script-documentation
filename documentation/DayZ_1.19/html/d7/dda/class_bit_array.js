var class_bit_array =
[
    [ "AddIDToMask", "d7/dda/class_bit_array.html#a1216d72ba88d1ec0622766e030c6d343", null ],
    [ "CreateArrayWithID", "d7/dda/class_bit_array.html#af8bd317b9702772876733e8aa40aa573", null ],
    [ "CreateMaskArray", "d7/dda/class_bit_array.html#a19ff1ae25897d0ba0108cf9875b126f7", null ],
    [ "GetBitCount", "d7/dda/class_bit_array.html#aff26f1d791a1a0a2ec6e089f8f896030", null ],
    [ "IDToIndex", "d7/dda/class_bit_array.html#a04e6e1086bd53f223674fb71872df114", null ],
    [ "IDToMask", "d7/dda/class_bit_array.html#addd3016adef7f764b93622a91ff42bfc", null ],
    [ "IDToNumOfItems", "d7/dda/class_bit_array.html#a8ba987eb3174bb6208755e90b32815d7", null ],
    [ "IsMaskContainID", "d7/dda/class_bit_array.html#a4ceaaa9b6d7fdecd39b45c290196d610", null ]
];