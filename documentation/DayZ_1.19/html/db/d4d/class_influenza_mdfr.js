var class_influenza_mdfr =
[
    [ "ActivateCondition", "db/d4d/class_influenza_mdfr.html#acbab3556d1b7a798c40349abc6342ef8", null ],
    [ "DeactivateCondition", "db/d4d/class_influenza_mdfr.html#a9a1cbd14ffec902ea9cecf94473e1945", null ],
    [ "GetDebugText", "db/d4d/class_influenza_mdfr.html#a7bf60ce21df41e38c486754572e35fa2", null ],
    [ "Init", "db/d4d/class_influenza_mdfr.html#ae63168da675f36426bae6f69468b8a17", null ],
    [ "OnActivate", "db/d4d/class_influenza_mdfr.html#a02810ed7c024c5992eb26cc1ec82bf3f", null ],
    [ "OnDeactivate", "db/d4d/class_influenza_mdfr.html#a9ed403043512d40527da02eeb53c9a6b", null ],
    [ "OnTick", "db/d4d/class_influenza_mdfr.html#a690df00c56731c41d0b75e7440e290dc", null ],
    [ "AGENT_THRESHOLD_ACTIVATE", "db/d4d/class_influenza_mdfr.html#a56030d3960847d3edd8d36a038f0ecf8", null ],
    [ "AGENT_THRESHOLD_DEACTIVATE", "db/d4d/class_influenza_mdfr.html#a6d9aac042adcf4df23c859690d7d8fbb", null ]
];