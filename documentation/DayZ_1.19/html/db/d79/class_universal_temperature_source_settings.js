var class_universal_temperature_source_settings =
[
    [ "m_AffectStat", "db/d79/class_universal_temperature_source_settings.html#ac6485ca959fa11eb6d967f39ba996fcf", null ],
    [ "m_ManualUpdate", "db/d79/class_universal_temperature_source_settings.html#a88d3a0dce678c2bd38890b1f100f2471", null ],
    [ "m_Parent", "db/d79/class_universal_temperature_source_settings.html#ad1b055fe3005307718cd77b2a51e24be", null ],
    [ "m_Position", "db/d79/class_universal_temperature_source_settings.html#a017eb94f472ab0dc53a8f8c7e4eb0c0d", null ],
    [ "m_RangeFull", "db/d79/class_universal_temperature_source_settings.html#a3bc2e904092203533388a4d0bda0bedb", null ],
    [ "m_RangeMax", "db/d79/class_universal_temperature_source_settings.html#a5aedd8f55c240e57654e2c1865e63266", null ],
    [ "m_TemperatureCap", "db/d79/class_universal_temperature_source_settings.html#a3d898b28c8dc0bffd2fe144fddffe538", null ],
    [ "m_TemperatureMax", "db/d79/class_universal_temperature_source_settings.html#abcf789c159e76141f1e4a2faedfb749e", null ],
    [ "m_TemperatureMin", "db/d79/class_universal_temperature_source_settings.html#a93a985cf4cad2733d89bdcd0254fbc68", null ],
    [ "m_Updateable", "db/d79/class_universal_temperature_source_settings.html#a389941b6b5787defee17703afc1a938d", null ],
    [ "m_UpdateInterval", "db/d79/class_universal_temperature_source_settings.html#a394f56e0a4da4604b8b8228fcc49ff9b", null ]
];