var class_color_values_data =
[
    [ "ColorValuesData", "d0/db0/class_color_values_data.html#a3eca79368a1824bfd65446403c268684", null ],
    [ "GetOperator", "d0/db0/class_color_values_data.html#aa96da2bf33d17c5fc259a8dbf06e5848", null ],
    [ "GetValues", "d0/db0/class_color_values_data.html#aa28a0c414bcfbe7eb5ec4fed89c8344d", null ],
    [ "SetOperator", "d0/db0/class_color_values_data.html#a3b8c4171ba3cd6bca92b169adf15076d", null ],
    [ "SetValues", "d0/db0/class_color_values_data.html#af4c22b9cccee1ffdbb02534a660659dd", null ],
    [ "m_Operator", "d0/db0/class_color_values_data.html#a365a3dd0b71ef6f3689476b26ca91ebf", null ],
    [ "m_Values", "d0/db0/class_color_values_data.html#a964ec56971e88f3516fd45b627d1abe0", null ]
];