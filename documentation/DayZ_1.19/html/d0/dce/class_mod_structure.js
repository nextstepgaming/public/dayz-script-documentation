var class_mod_structure =
[
    [ "ModStructure", "d0/dce/class_mod_structure.html#ace52bc9ab2d35886e7481848928795f5", null ],
    [ "GetModActionURL", "d0/dce/class_mod_structure.html#a43c73629451caf4cbbb67bb4339d9bbe", null ],
    [ "GetModLogo", "d0/dce/class_mod_structure.html#abee8ba82a934179327eda3cf481bbb6b", null ],
    [ "GetModLogoOver", "d0/dce/class_mod_structure.html#a837a302007c43736d4a17e28a6809301", null ],
    [ "GetModLogoSmall", "d0/dce/class_mod_structure.html#af370dbc52ad93519a5ede9fcfeb941d8", null ],
    [ "GetModName", "d0/dce/class_mod_structure.html#ab09658ed2168df22ac78335c15af8955", null ],
    [ "GetModOverview", "d0/dce/class_mod_structure.html#ac2bd393a193940fc4d4e462b621c5b19", null ],
    [ "GetModToltip", "d0/dce/class_mod_structure.html#ab4398cb2dd1cfd4229cf02b62995662e", null ],
    [ "LoadData", "d0/dce/class_mod_structure.html#a47577535f2defd44b2dad119925007e1", null ],
    [ "m_ModActionURL", "d0/dce/class_mod_structure.html#afa8a6bc414dd094677d2bd8cf96a4756", null ],
    [ "m_ModIndex", "d0/dce/class_mod_structure.html#a7f51607bab73d03ce92a6a5e0665233d", null ],
    [ "m_ModLogo", "d0/dce/class_mod_structure.html#abb49ab70de80d0e2048a75de8e336e8c", null ],
    [ "m_ModLogoOver", "d0/dce/class_mod_structure.html#a94010d28c6bbc54de5767d6b2ee26f34", null ],
    [ "m_ModLogoSmall", "d0/dce/class_mod_structure.html#a5eabd75607cabdaa4a19c01c1814a639", null ],
    [ "m_ModName", "d0/dce/class_mod_structure.html#a419d08e51abec1ba4ba92ecd8f27ec07", null ],
    [ "m_ModOverview", "d0/dce/class_mod_structure.html#ae5cc42d02ab63efa4f398ab8d490c5d7", null ],
    [ "m_ModPath", "d0/dce/class_mod_structure.html#a761159f795d0c1ab44558ad24d7a3568", null ],
    [ "m_ModTooltip", "d0/dce/class_mod_structure.html#a42468f2f52161f952e0972d2e0714adf", null ]
];