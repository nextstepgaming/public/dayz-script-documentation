var class_immune_system_mdfr =
[
    [ "ActivateCondition", "d6/d8f/class_immune_system_mdfr.html#a519567530108a31e1098e8b97e464d5f", null ],
    [ "DeactivateCondition", "d6/d8f/class_immune_system_mdfr.html#a634b77b1989a062507dda59850fa19bd", null ],
    [ "Init", "d6/d8f/class_immune_system_mdfr.html#aece45847537c73fdd116b3eaf3d4eb4a", null ],
    [ "OnActivate", "d6/d8f/class_immune_system_mdfr.html#a42a199dd97b71015c6450369453aeaf8", null ],
    [ "OnReconnect", "d6/d8f/class_immune_system_mdfr.html#a1b7e920dbd56759fd5ccf3e39437796a", null ],
    [ "OnTick", "d6/d8f/class_immune_system_mdfr.html#a5b3a7a1172c337f12090e09b70060d09", null ],
    [ "m_HasDisease", "d6/d8f/class_immune_system_mdfr.html#a4082466a27aeae6854b9d81f957b6eb9", null ],
    [ "m_HasHealings", "d6/d8f/class_immune_system_mdfr.html#af4a9fbc5ac592510cefb049c3859f1f7", null ]
];