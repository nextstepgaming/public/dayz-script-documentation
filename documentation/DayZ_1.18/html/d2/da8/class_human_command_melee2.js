var class_human_command_melee2 =
[
    [ "HumanCommandMelee2", "d2/da8/class_human_command_melee2.html#a69b524713ac349ed6150b42dbb6ddfae", null ],
    [ "~HumanCommandMelee2", "d2/da8/class_human_command_melee2.html#a251147f5cb7464fffe2f25dd96727975", null ],
    [ "Cancel", "d2/da8/class_human_command_melee2.html#ac9ffa66c546ab428f3c4e747424d714e", null ],
    [ "ContinueCombo", "d2/da8/class_human_command_melee2.html#ac919c9d912bc9c6c3aca79e2ef68bd76", null ],
    [ "GetComboCount", "d2/da8/class_human_command_melee2.html#a424d4b6baca160d32f4a5462974bece8", null ],
    [ "IsInComboRange", "d2/da8/class_human_command_melee2.html#aac33a2856e97c2cefebdf9de3f94d29a", null ],
    [ "IsOnBack", "d2/da8/class_human_command_melee2.html#ae26f17f8d8b14c29f1b1145e8b3fcaa6", null ],
    [ "WasHit", "d2/da8/class_human_command_melee2.html#a524f4ddad52cba47558e2d483cb89dea", null ],
    [ "HIT_TYPE_FINISHER", "d2/da8/class_human_command_melee2.html#a606c5d5654ff1f9c561b80f5868904aa", null ],
    [ "HIT_TYPE_HEAVY", "d2/da8/class_human_command_melee2.html#a621a064f10e6fed060346a74828de5f4", null ],
    [ "HIT_TYPE_LIGHT", "d2/da8/class_human_command_melee2.html#a64aef66e238902312542d9aa54ba742c", null ]
];