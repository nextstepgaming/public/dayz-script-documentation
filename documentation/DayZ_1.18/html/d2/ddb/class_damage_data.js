var class_damage_data =
[
    [ "DamageData", "d2/ddb/class_damage_data.html#ad6f4c159f7fb8ac702cc7692b923ab89", null ],
    [ "GetValueBlood", "d2/ddb/class_damage_data.html#a92acbb1efa4ab556eb006108547a90e0", null ],
    [ "GetValueGlobal", "d2/ddb/class_damage_data.html#acbcb62b4d9ac8d68df3454645d256bfe", null ],
    [ "GetValueShock", "d2/ddb/class_damage_data.html#a463c63b961b66d52962ebf5153537ac3", null ],
    [ "m_ValueBlood", "d2/ddb/class_damage_data.html#acf223b0cc276ec7f03d385baafc25a4b", null ],
    [ "m_ValueGlobal", "d2/ddb/class_damage_data.html#a352cb70a65353414908e28cffad1b242", null ],
    [ "m_ValueShock", "d2/ddb/class_damage_data.html#ab9d430cc903dec8b914eafb259920a49", null ]
];