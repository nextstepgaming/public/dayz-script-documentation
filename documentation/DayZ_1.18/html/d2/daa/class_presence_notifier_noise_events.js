var class_presence_notifier_noise_events =
[
    [ "PresenceNotifierNoiseEvents", "d2/daa/class_presence_notifier_noise_events.html#ab2f8f7dac85915cee461b2172859e902", null ],
    [ "GetValue", "d2/daa/class_presence_notifier_noise_events.html#ae5f37800fe5069f2b74d78f740f8c72d", null ],
    [ "ProcessEvent", "d2/daa/class_presence_notifier_noise_events.html#ae4fee471e11fc4efb74255fe7fd5b635", null ],
    [ "RegisterEvent", "d2/daa/class_presence_notifier_noise_events.html#aa63fa1080b10e4ba96179c6dda1b41d1", null ],
    [ "ResetEvent", "d2/daa/class_presence_notifier_noise_events.html#a1b98e6f97cec8c5d0ce64a84cebd0cfd", null ],
    [ "m_CooldownTimer", "d2/daa/class_presence_notifier_noise_events.html#ab393752ac720c6d9a25f359d0167ea43", null ],
    [ "m_PresenceNotifierNotifierEvents", "d2/daa/class_presence_notifier_noise_events.html#a4d91c443657db74992ca8ffd7a5a61f0", null ],
    [ "m_Value", "d2/daa/class_presence_notifier_noise_events.html#ab9eb3de7e23bbccd48fccd708b1e5a19", null ]
];