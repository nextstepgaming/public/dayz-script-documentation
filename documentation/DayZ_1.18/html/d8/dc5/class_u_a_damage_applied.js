var class_u_a_damage_applied =
[
    [ "BUILD", "d8/dc5/class_u_a_damage_applied.html#a17e3bef2588976cd8d00fd4b52230fbe", null ],
    [ "DEFUSE_TOOLS", "d8/dc5/class_u_a_damage_applied.html#a04f30545a65a77ea3da0722ba7cf7396", null ],
    [ "DESTROY", "d8/dc5/class_u_a_damage_applied.html#aa6e0ecae4a329f86ec4a7782b0418342", null ],
    [ "DISMANTLE", "d8/dc5/class_u_a_damage_applied.html#ad102dcae5d29ef4fc42376be146a8c19", null ],
    [ "REPAIR", "d8/dc5/class_u_a_damage_applied.html#a50aa4c2e82022f5f75e8edaeb57d8507", null ],
    [ "SAW_LOCK", "d8/dc5/class_u_a_damage_applied.html#a6f14fb8f347cbccd5860b3758a7552a3", null ],
    [ "SAW_PLANKS", "d8/dc5/class_u_a_damage_applied.html#a46316543ae31f91b5d5aa04739305cde", null ],
    [ "SKINNING", "d8/dc5/class_u_a_damage_applied.html#a78c68eb84d103f72e090b8c67e647b68", null ]
];