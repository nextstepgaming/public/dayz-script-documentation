var class_car_light_base =
[
    [ "CarRearLight", "d8/d66/class_car_light_base.html#a3c741937b7da8f1aa141ec64fb86bcdc", null ],
    [ "CivilianSedanFrontLight", "d8/d66/class_car_light_base.html#a49ccfe8fd0188cfee4ae5752461cef5e", null ],
    [ "Hatchback_02FrontLight", "d8/d66/class_car_light_base.html#a832c6c14196fae8742383621f4ced901", null ],
    [ "OffroadHatchbackFrontLight", "d8/d66/class_car_light_base.html#a315af72b593840f46e230baf1c527f3b", null ],
    [ "Sedan_02FrontLight", "d8/d66/class_car_light_base.html#a8491a79fbd720469d3f2751565fb3459", null ],
    [ "SetAsSegregatedBrakeLight", "d8/d66/class_car_light_base.html#aeb7e17cc9c8512dc7b0f785f20e64650", null ],
    [ "SetAsSegregatedReverseLight", "d8/d66/class_car_light_base.html#ab6bb8ec5f114e628e7ff997943c7c669", null ],
    [ "Truck_01FrontLight", "d8/d66/class_car_light_base.html#a6b9e55716f2b690bfc51bc9331ce1b56", null ],
    [ "m_SegregatedBrakeAngle", "d8/d66/class_car_light_base.html#a2aee45da918412b34d0f7128a0c098a7", null ],
    [ "m_SegregatedBrakeBrightness", "d8/d66/class_car_light_base.html#a645fc31fd9e0459ca6ebea0c1f36067a", null ],
    [ "m_SegregatedBrakeColorRGB", "d8/d66/class_car_light_base.html#a9b580661e6ec42d90c9772dfb74998ec", null ],
    [ "m_SegregatedBrakeRadius", "d8/d66/class_car_light_base.html#acde429811d5d4b10cf261db45ecd1139", null ]
];