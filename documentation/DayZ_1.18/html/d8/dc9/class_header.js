var class_header =
[
    [ "Header", "d8/dc9/class_header.html#af88b2e65b486ecba6d4c9d6441256ce0", null ],
    [ "DraggingOverHeader", "d8/dc9/class_header.html#a307e84350e1a1c324a1b7bec21b52a8d", null ],
    [ "OnDropReceivedFromHeader", "d8/dc9/class_header.html#a0293582f586dc959c91dad536946a297", null ],
    [ "SetActive", "d8/dc9/class_header.html#a8f131d3dc4856e42156a5e6da477fb21", null ],
    [ "SetItemPreview", "d8/dc9/class_header.html#a636cb0b668decfcbf8210017a19e1e55", null ],
    [ "SetName", "d8/dc9/class_header.html#a670282fbdd3a0735fb0b32564f06641f", null ],
    [ "m_CollapseButton", "d8/dc9/class_header.html#acf5dce832ee6d0b26d6b5bd3740199f2", null ],
    [ "m_DefaultColor", "d8/dc9/class_header.html#aac033f8a207e64037563c100e73a1a83", null ],
    [ "m_DefaultFontSize", "d8/dc9/class_header.html#a6b4656850f00128dab241ac8aa2381b8", null ],
    [ "m_Entity", "d8/dc9/class_header.html#a82bcf871c61641de21f936b6452468f0", null ],
    [ "m_HeaderText", "d8/dc9/class_header.html#a86bd586b4b389e9c73260bb8baa2e85e", null ]
];