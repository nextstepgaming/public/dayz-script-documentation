var class_plugin_manager =
[
    [ "PluginManager", "d8/df1/class_plugin_manager.html#ad38d47d51c0389d37dad3c5b7f0d8c60", null ],
    [ "~PluginManager", "d8/df1/class_plugin_manager.html#a29003afd7f846d5ec263997ad44b905d", null ],
    [ "GetPluginByType", "d8/df1/class_plugin_manager.html#a062c82d1db6bfbb8e4746ab20ac2b5db", null ],
    [ "Init", "d8/df1/class_plugin_manager.html#ac5de560435cade9f36e667304298ffa2", null ],
    [ "MainOnUpdate", "d8/df1/class_plugin_manager.html#ab5d2987b57f79730fba72837a1079a55", null ],
    [ "PluginsInit", "d8/df1/class_plugin_manager.html#a79c5a3ed6249ba7881ee5cffcdf99292", null ],
    [ "RegisterPlugin", "d8/df1/class_plugin_manager.html#a472b8e77b7e8f7ad9eaf7d8b2b46e21f", null ],
    [ "RegisterPluginDebug", "d8/df1/class_plugin_manager.html#a4ccabd3d674e5f3d0c6e65df196310e1", null ],
    [ "UnregisterPlugin", "d8/df1/class_plugin_manager.html#ad7ba94bebdae5370ba207cab3b1916f0", null ],
    [ "m_PluginRegister", "d8/df1/class_plugin_manager.html#a59591e32b7703878cb4e34d761d38c15", null ],
    [ "m_PluginsPtrs", "d8/df1/class_plugin_manager.html#ad1410f1da453a2f988b16b7e223850dd", null ]
];