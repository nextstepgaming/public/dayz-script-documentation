var dir_f28e0f4933179a31e3883dbb29b7f996 =
[
    [ "Camping", "dir_f8c67f46fbab81a6bcdbef5e6c3a6a07.html", "dir_f8c67f46fbab81a6bcdbef5e6c3a6a07" ],
    [ "Consumables", "dir_c069bbfd036620ec5b78accfd518157c.html", "dir_c069bbfd036620ec5b78accfd518157c" ],
    [ "Containers", "dir_11d0d679ca63c95e60846f9553de890d.html", "dir_11d0d679ca63c95e60846f9553de890d" ],
    [ "Crafting", "dir_810734c95a5e0f0ac8d38a3944f258c0.html", "dir_810734c95a5e0f0ac8d38a3944f258c0" ],
    [ "Cultivation", "dir_9c8daa1687df4222ece61ed41e5a3509.html", "dir_9c8daa1687df4222ece61ed41e5a3509" ],
    [ "Drinks", "dir_9359970d088142d4116705af4ff23bd3.html", "dir_9359970d088142d4116705af4ff23bd3" ],
    [ "Food", "dir_2900ac54f457c665e7b743319b8ff5d8.html", "dir_2900ac54f457c665e7b743319b8ff5d8" ],
    [ "Medical", "dir_7fef9983aabe90dbb513b59d2414098b.html", "dir_7fef9983aabe90dbb513b59d2414098b" ],
    [ "Navigation", "dir_1f666c8fabecdac1e50a0255aae8ad96.html", "dir_1f666c8fabecdac1e50a0255aae8ad96" ],
    [ "Optics", "dir_e8d4f29121d6beae5ca60bd3eba1b1b8.html", "dir_e8d4f29121d6beae5ca60bd3eba1b1b8" ],
    [ "Tools", "dir_e36a2cddfdd187ac0db92f8d1eafd039.html", "dir_e36a2cddfdd187ac0db92f8d1eafd039" ]
];