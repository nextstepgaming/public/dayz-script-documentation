var class_notification_u_i =
[
    [ "NotificationUI", "da/da7/class_notification_u_i.html#ad1ccac105ab9cac255a21cb0f9384654", null ],
    [ "~NotificationUI", "da/da7/class_notification_u_i.html#adb3a7dc207043a5ea2256def746dfcbd", null ],
    [ "AddNotification", "da/da7/class_notification_u_i.html#a59be4c9c4bd749971f2623ceafc933e8", null ],
    [ "AddVoiceNotification", "da/da7/class_notification_u_i.html#a9e509ad23986fcdeae6aa9b6bd6acd43", null ],
    [ "ClearVoiceNotifications", "da/da7/class_notification_u_i.html#ac6551740004935faa982691ae3eab1e6", null ],
    [ "RemoveNotification", "da/da7/class_notification_u_i.html#aa7311dfa113606fc21358568d9663dd2", null ],
    [ "RemoveVoiceNotification", "da/da7/class_notification_u_i.html#ad1d56762afc60bc77e2cea287c2db91a", null ],
    [ "Update", "da/da7/class_notification_u_i.html#aaea4db785f13ebafdb0886afe86f7f93", null ],
    [ "UpdateTargetHeight", "da/da7/class_notification_u_i.html#a9838364f9f0bee0648e727d50b1c00cc", null ],
    [ "m_CurrentHeight", "da/da7/class_notification_u_i.html#a804eeca91ec2be6cf4df6ad0da9512c3", null ],
    [ "m_NotificationContent", "da/da7/class_notification_u_i.html#a8b45ccd2fabf9ec733e7ba60acacdc53", null ],
    [ "m_Notifications", "da/da7/class_notification_u_i.html#a1c0a3a76925df5006dd140b15162ac0d", null ],
    [ "m_Root", "da/da7/class_notification_u_i.html#a35ce64e67b197116333dffdbb42bcbee", null ],
    [ "m_Spacer", "da/da7/class_notification_u_i.html#a76a3d3ae7ff5eb1ebe5a88183e53d6d9", null ],
    [ "m_TargetHeight", "da/da7/class_notification_u_i.html#afdbcd4c2034be4195db07a92e6837ce9", null ],
    [ "m_VelArr", "da/da7/class_notification_u_i.html#ac89942678b2c69deccbd1f5f80afa40d", null ],
    [ "m_VoiceContent", "da/da7/class_notification_u_i.html#a48abbb4d00d89b608a451288ea8d1ddd", null ],
    [ "m_VoiceNotifications", "da/da7/class_notification_u_i.html#a2f4960ee3f9a5b7ff723d362696f5a1d", null ],
    [ "m_WidgetTimers", "da/da7/class_notification_u_i.html#a860b871e0ad70477178a11efea501abd", null ],
    [ "m_Width", "da/da7/class_notification_u_i.html#a564deb41167a1c7c88e1428c12e41908", null ]
];