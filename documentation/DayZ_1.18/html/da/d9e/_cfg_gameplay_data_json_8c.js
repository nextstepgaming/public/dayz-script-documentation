var _cfg_gameplay_data_json_8c =
[
    [ "CfgGameplayJson", "d9/df7/class_cfg_gameplay_json.html", "d9/df7/class_cfg_gameplay_json" ],
    [ "ITEM_GeneralData", "d1/deb/class_i_t_e_m___general_data.html", "d1/deb/class_i_t_e_m___general_data" ],
    [ "ITEM_PlayerData", "d9/ddf/class_i_t_e_m___player_data.html", "d9/ddf/class_i_t_e_m___player_data" ],
    [ "ITEM_ShockHandlingData", "d2/de4/class_i_t_e_m___shock_handling_data.html", "d2/de4/class_i_t_e_m___shock_handling_data" ],
    [ "ITEM_StaminaData", "df/dd4/class_i_t_e_m___stamina_data.html", "df/dd4/class_i_t_e_m___stamina_data" ],
    [ "ITEM_WorldData", "da/dee/class_i_t_e_m___world_data.html", "da/dee/class_i_t_e_m___world_data" ],
    [ "ITEM_BaseBuildingData", "d4/d96/class_i_t_e_m___base_building_data.html", "d4/d96/class_i_t_e_m___base_building_data" ],
    [ "ITEM_HologramData", "d9/dda/class_i_t_e_m___hologram_data.html", "d9/dda/class_i_t_e_m___hologram_data" ],
    [ "ITEM_ConstructionData", "d7/d8a/class_i_t_e_m___construction_data.html", "d7/d8a/class_i_t_e_m___construction_data" ],
    [ "ITEM_UIData", "dc/d0c/class_i_t_e_m___u_i_data.html", "dc/d0c/class_i_t_e_m___u_i_data" ],
    [ "ITEM_HitIndicationData", "d1/daa/class_i_t_e_m___hit_indication_data.html", "d1/daa/class_i_t_e_m___hit_indication_data" ]
];