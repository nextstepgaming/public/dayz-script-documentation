var class_connection_lost =
[
    [ "ConnectionLost", "df/ddf/class_connection_lost.html#aa9ac9ed6d50c4819883b51943d5f7aab", null ],
    [ "GetDuration", "df/ddf/class_connection_lost.html#a93e44fc0bcbe2a16463301d7732d4873", null ],
    [ "Hide", "df/ddf/class_connection_lost.html#ab6d9c0b00209d68e2b6808809e719ce3", null ],
    [ "SetDuration", "df/ddf/class_connection_lost.html#a29a378b2194c29b53ba795143554c7f0", null ],
    [ "SetText", "df/ddf/class_connection_lost.html#ad594fbeb0a2f4375661477b05331e6b3", null ],
    [ "Show", "df/ddf/class_connection_lost.html#aebee873f4a3831a8d48a8feb85c1a05e", null ],
    [ "m_duration", "df/ddf/class_connection_lost.html#a2d900e30a57a5de4ad193202b5403c3d", null ],
    [ "m_TextWidgetTitle", "df/ddf/class_connection_lost.html#ac7b06d04665419ab7d97f5fa5150b14c", null ],
    [ "m_WidgetRoot", "df/ddf/class_connection_lost.html#a36282d0a0670e8bc21fc09899b20d70d", null ]
];