var class_get_first_server_with_empty_slot_input =
[
    [ "SetGameVersion", "df/d9c/class_get_first_server_with_empty_slot_input.html#a2ef37cbf990ce87676eb3722314dd1b1", null ],
    [ "SetOfficial", "df/d9c/class_get_first_server_with_empty_slot_input.html#ae947970071dc0937f573c4018d7a742d", null ],
    [ "SetRegionId", "df/d9c/class_get_first_server_with_empty_slot_input.html#a29b13c1351246aa086b53b1dbdfaca62", null ],
    [ "m_GameVersion", "df/d9c/class_get_first_server_with_empty_slot_input.html#ae40361c5e452749b57caeb7d5efbe417", null ],
    [ "m_Official", "df/d9c/class_get_first_server_with_empty_slot_input.html#a65796ce11e8583e668f1f18cff0c98be", null ],
    [ "m_RegionId", "df/d9c/class_get_first_server_with_empty_slot_input.html#ad7b61944c6e69a920baf1f728ff995a8", null ],
    [ "m_UseGameVersion", "df/d9c/class_get_first_server_with_empty_slot_input.html#a2541131026978f73537f7fc94c7492b3", null ],
    [ "m_UseOfficial", "df/d9c/class_get_first_server_with_empty_slot_input.html#a9ada71b1b6a76af3a657042fb4ea13b0", null ],
    [ "m_UseRegionId", "df/d9c/class_get_first_server_with_empty_slot_input.html#a7eff62b87126a7479d898e7a11654a28", null ]
];