var class_player_identity =
[
    [ "~PlayerIdentity", "df/d11/class_player_identity.html#a33afb2a3745eb6fd22ee362370436e38", null ],
    [ "GetBandwidthAvg", "df/d11/class_player_identity.html#a93d513b07a60719c45e57b4bfd55a464", null ],
    [ "GetBandwidthMax", "df/d11/class_player_identity.html#a8748777cda305b3211255b5ec1c09489", null ],
    [ "GetBandwidthMin", "df/d11/class_player_identity.html#addf3cf09325f535b9639befa8982ecda", null ],
    [ "GetFullName", "df/d11/class_player_identity.html#a5527253d2cf9086c695ad94fcbcbd211", null ],
    [ "GetId", "df/d11/class_player_identity.html#ac53c7c6eba4162fac4c035d13b1cb141", null ],
    [ "GetName", "df/d11/class_player_identity.html#ae8c0f77a32dbb11b4be3ec2a55b3c062", null ],
    [ "GetPingAvg", "df/d11/class_player_identity.html#a1ef6aeb71a546625f52e87e66b66df68", null ],
    [ "GetPingMax", "df/d11/class_player_identity.html#a749a4be1315d1946946b81f915952c80", null ],
    [ "GetPingMin", "df/d11/class_player_identity.html#a06d34e59f6d5361300cb83e1835fbe41", null ],
    [ "GetPlainId", "df/d11/class_player_identity.html#adb26023c6a08029b785fdc1c9f259f46", null ],
    [ "GetPlayerId", "df/d11/class_player_identity.html#a5ed4a0af66349b845cf4137f20eab176", null ]
];