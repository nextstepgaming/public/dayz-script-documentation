var class_kiwi =
[
    [ "CanBeCooked", "d9/dbc/class_kiwi.html#aeb0951faf8dd5146be1e88f1c25b36a4", null ],
    [ "CanBeCookedOnStick", "d9/dbc/class_kiwi.html#aabeffb40e28e2822586389fc72662abd", null ],
    [ "CanDecay", "d9/dbc/class_kiwi.html#a96a5c165cff832088bd1db8c7fe462bf", null ],
    [ "IsFruit", "d9/dbc/class_kiwi.html#a357bf97a9d9f68fe6aae04698a7cd64c", null ],
    [ "SetActions", "d9/dbc/class_kiwi.html#afd5da274fb59d1cd5736f079ffd18ba3", null ]
];