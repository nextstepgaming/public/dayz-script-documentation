var class_remote_player_meta =
[
    [ "RemotePlayerMeta", "d0/d05/class_remote_player_meta.html#ac20425cff3a9f286d3f60ff74a00539f", null ],
    [ "GetDebugType", "d0/d05/class_remote_player_meta.html#a480f7f1b66d5fd2e2f042a89d422c67c", null ],
    [ "GetPlayer", "d0/d05/class_remote_player_meta.html#aee18784f5bb961e338371e05519e1f21", null ],
    [ "SetDebugType", "d0/d05/class_remote_player_meta.html#ad55784a8b08d0a147b2ada8c352292f3", null ],
    [ "SetPlayer", "d0/d05/class_remote_player_meta.html#aae4a76b7af304c25e661073063d445de", null ],
    [ "m_DebugType", "d0/d05/class_remote_player_meta.html#ade2306e4504c6ef3730db15a2a58a23e", null ],
    [ "m_Player", "d0/d05/class_remote_player_meta.html#aa73142007939797b206c421caa245c72", null ]
];