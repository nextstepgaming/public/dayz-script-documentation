var class_selection_translation =
[
    [ "SelectionTranslation", "d0/df1/class_selection_translation.html#af335c636bd814e51e5ed04892913d91a", null ],
    [ "GetSelectionState", "d0/df1/class_selection_translation.html#af581affea636579b05a41cb2eefedbe1", null ],
    [ "GetTranslatedSelections", "d0/df1/class_selection_translation.html#aed9b63b0a30b53e240981a27dc8c15e8", null ],
    [ "InitTranslatedSelections", "d0/df1/class_selection_translation.html#a7d3c4cc03ae3382eb9d6959146148e43", null ],
    [ "SearchAndTranslate", "d0/df1/class_selection_translation.html#a6761c0aff271da8c1fc3c00c9ce2de63", null ],
    [ "SetSelectionState", "d0/df1/class_selection_translation.html#ae8d68f9b2ccaf977c440cfb534804583", null ],
    [ "m_BasePath", "d0/df1/class_selection_translation.html#a28eabd77353e5dc1021389c035af4055", null ],
    [ "m_Head", "d0/df1/class_selection_translation.html#aa9ec6c6f73fd3bb971f6699abd86b1df", null ],
    [ "m_SelectionState", "d0/df1/class_selection_translation.html#ae6f9900a2cc0cc2f7155249945d9c9cb", null ],
    [ "m_TranslatedSelections", "d0/df1/class_selection_translation.html#aa4c743114bf46d0b0e45cb18350cfe7a", null ],
    [ "m_TranslationFinished", "d0/df1/class_selection_translation.html#aff4339506cead891b5897996f6e1c73b", null ]
];