var class_display_element_tendency =
[
    [ "TendencyBacteria", "d0/dea/class_display_element_tendency.html#a6a4eeb40d95ea36539280fb60aaa6ee2", null ],
    [ "TendencyBlood", "d0/dea/class_display_element_tendency.html#add70eba7e602825e4e864cc6f47a3be8", null ],
    [ "TendencyHealth", "d0/dea/class_display_element_tendency.html#ae07bba0fa77350e9fa49904edab54198", null ],
    [ "TendencyHunger", "d0/dea/class_display_element_tendency.html#a1f58033fec82295db3e5bc9ba2b621bc", null ],
    [ "TendencyTemperature", "d0/dea/class_display_element_tendency.html#a056002a9cd962600b424066c1ae476dd", null ],
    [ "TendencyThirst", "d0/dea/class_display_element_tendency.html#a3058db3181ae6abc8b2068e546008b68", null ],
    [ "TranslateLevelToStatus", "d0/dea/class_display_element_tendency.html#aa69b06c9d5c6c9c1db4a59376c4195d3", null ]
];