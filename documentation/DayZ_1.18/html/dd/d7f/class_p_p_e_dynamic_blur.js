var class_p_p_e_dynamic_blur =
[
    [ "GetDefaultMaterialPath", "dd/d7f/class_p_p_e_dynamic_blur.html#acfef8aa0d0d206f949f807ab8582b6bb", null ],
    [ "GetPostProcessEffectID", "dd/d7f/class_p_p_e_dynamic_blur.html#a4913447dbd001418c02785c3139be9b3", null ],
    [ "RegisterMaterialParameters", "dd/d7f/class_p_p_e_dynamic_blur.html#ac1fd6008a854ec4fad95a26098950f37", null ],
    [ "PARAM_BLURRINESS", "dd/d7f/class_p_p_e_dynamic_blur.html#a07f0338dd763e2c216e9cdb59f6f000c", null ]
];