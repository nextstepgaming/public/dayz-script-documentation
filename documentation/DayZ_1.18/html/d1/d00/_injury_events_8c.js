var _injury_events_8c =
[
    [ "PlayerSoundEventBase", "d5/def/class_player_sound_event_base.html", "d5/def/class_player_sound_event_base" ],
    [ "InjurySoundEvents", "d0/d2b/class_injury_sound_events.html", "d0/d2b/class_injury_sound_events" ],
    [ "CanPlay", "d1/d00/_injury_events_8c.html#a7604fb09a52adcc0e44c5ec15d023d85", null ],
    [ "HasPriorityOverCurrent", "d1/d00/_injury_events_8c.html#abd92adfba0c07af47e30e426acdada93", null ],
    [ "InjuryHeavySoundEvent", "d1/d00/_injury_events_8c.html#a8a083f088f8c96ebc7f804a1567ead9f", null ],
    [ "InjuryLightSoundEvent", "d1/d00/_injury_events_8c.html#adf78355a3910926e67f1ad2f46466286", null ],
    [ "InjuryMediumSoundEvent", "d1/d00/_injury_events_8c.html#af1650583c8505bc2a8d16ab353fd8f97", null ],
    [ "InjurySoundEvents", "d1/d00/_injury_events_8c.html#afad9087ad743b8dc41d38060452540dc", null ]
];