var class_synced_value_modifier =
[
    [ "SyncedValueModifier", "d4/db4/class_synced_value_modifier.html#ad3e322702ca1ccbcd3f04d1def4d156b", null ],
    [ "GetActive", "d4/db4/class_synced_value_modifier.html#aad3ec5ca09c987db7439e1c5f6f49caa", null ],
    [ "GetID", "d4/db4/class_synced_value_modifier.html#a3140269281fb0b2ea379fa72fded5a9f", null ],
    [ "GetLocked", "d4/db4/class_synced_value_modifier.html#a003e9884cdb1416d5a64d4b7dfb4442e", null ],
    [ "GetName", "d4/db4/class_synced_value_modifier.html#ade937414703fc2587f2443f34ff0ae04", null ],
    [ "m_Active", "d4/db4/class_synced_value_modifier.html#accb119b9856e09c1a181736c09eddfff", null ],
    [ "m_ID", "d4/db4/class_synced_value_modifier.html#a807b55c5e392a84eea1c4a2bbbda2ed5", null ],
    [ "m_Locked", "d4/db4/class_synced_value_modifier.html#ad652a1bf5530b08c51f835538b9fb01b", null ],
    [ "m_Name", "d4/db4/class_synced_value_modifier.html#a9da635306ef6bb51efc2fbdbd11cce47", null ]
];