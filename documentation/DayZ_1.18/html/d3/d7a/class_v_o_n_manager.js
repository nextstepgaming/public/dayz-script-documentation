var class_v_o_n_manager =
[
    [ "CleanupInstance", "d3/d7a/class_v_o_n_manager.html#a7b7857e936ecb1dd185372249757647d", null ],
    [ "GetInstance", "d3/d7a/class_v_o_n_manager.html#a3950353d469b51197cc079a0ab36996a", null ],
    [ "Init", "d3/d7a/class_v_o_n_manager.html#ad0289a543c5ed75205b56bdb71f1d1e7", null ],
    [ "IsVoiceThresholdMinimum", "d3/d7a/class_v_o_n_manager.html#ad1b1c96249c41ea555afba9b44be0a6d", null ],
    [ "IsVONToggled", "d3/d7a/class_v_o_n_manager.html#a3126a02a9423a072d666a7164b6e5741", null ],
    [ "m_VONManager", "d3/d7a/class_v_o_n_manager.html#a50711c5fb2b50222c52bf5f67dbf6809", null ]
];