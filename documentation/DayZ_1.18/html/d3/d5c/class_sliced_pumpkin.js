var class_sliced_pumpkin =
[
    [ "CanBeCooked", "d3/d5c/class_sliced_pumpkin.html#a874d475b2578fab2ade87143bcefe509", null ],
    [ "CanBeCookedOnStick", "d3/d5c/class_sliced_pumpkin.html#a33a5f1c50202229ff406624c04a580cc", null ],
    [ "CanDecay", "d3/d5c/class_sliced_pumpkin.html#a99f9657770f14346468a9d28b903b75c", null ],
    [ "IsFruit", "d3/d5c/class_sliced_pumpkin.html#aa677d18df723355e955fea9ef9b460fa", null ],
    [ "SetActions", "d3/d5c/class_sliced_pumpkin.html#aa5e7722acb99b6742540784790d4d58c", null ]
];