var class_nutritional_profile =
[
    [ "NutritionalProfile", "d5/de5/class_nutritional_profile.html#a7db7f6793eaf3682197a841e176faa57", null ],
    [ "GetAgents", "d5/de5/class_nutritional_profile.html#a8a6aaea739f3a3508b208702967b9cc0", null ],
    [ "GetDigestibility", "d5/de5/class_nutritional_profile.html#a787aeb228bd11dc2029ec997e8b8bf51", null ],
    [ "GetEnergy", "d5/de5/class_nutritional_profile.html#a9cd99130b6d8e66e78c1789b26e10c77", null ],
    [ "GetFullnessIndex", "d5/de5/class_nutritional_profile.html#a958f47f8bfc51fe57ae0888ccd83570a", null ],
    [ "GetLiquidClassname", "d5/de5/class_nutritional_profile.html#aeee6e58ab008034cd49fd67974874722", null ],
    [ "GetLiquidType", "d5/de5/class_nutritional_profile.html#aa0a18aec25ee55597d4f5b1891082832", null ],
    [ "GetNutritionalIndex", "d5/de5/class_nutritional_profile.html#a30f4bd32c893e4741c3f2fc696320d74", null ],
    [ "GetToxicity", "d5/de5/class_nutritional_profile.html#aaf629292e146634586cad9af78315137", null ],
    [ "GetWaterContent", "d5/de5/class_nutritional_profile.html#a8035f35bae47ebd93c1a37b45fc6bd1a", null ],
    [ "IsLiquid", "d5/de5/class_nutritional_profile.html#a4221d7aaac68a689e274bc365ceaafa4", null ],
    [ "MarkAsLiquid", "d5/de5/class_nutritional_profile.html#a2e27dcd95f7f4b383953e07d6f5166b7", null ],
    [ "m_Agents", "d5/de5/class_nutritional_profile.html#a81cbf51cfdd06a4fe6e9fcdd54168a8f", null ],
    [ "m_Digestibility", "d5/de5/class_nutritional_profile.html#a12aa6abb617466596fe2b2ee6a84daf3", null ],
    [ "m_Energy", "d5/de5/class_nutritional_profile.html#a6d6483ebc75b49d14e2edef6b22440b9", null ],
    [ "m_FullnessIndex", "d5/de5/class_nutritional_profile.html#a4b665edd317097230cda054ed2acb616", null ],
    [ "m_LiquidClassname", "d5/de5/class_nutritional_profile.html#a49c07007c0b45ddd19483f630fb99710", null ],
    [ "m_LiquidType", "d5/de5/class_nutritional_profile.html#a294e643a5a6c8e72536cb69300ce716a", null ],
    [ "m_NutritionalIndex", "d5/de5/class_nutritional_profile.html#ae7bc2c6e752392310158000816015e7c", null ],
    [ "m_Toxicity", "d5/de5/class_nutritional_profile.html#a5dfe4f293d800ca2027ad9660cfb8bf2", null ],
    [ "m_WaterContent", "d5/de5/class_nutritional_profile.html#a44314a52143a76145c5e2035f73437be", null ]
];