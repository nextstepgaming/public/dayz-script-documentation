var class_static_transmitter =
[
    [ "IsStaticTransmitter", "dc/de8/class_static_transmitter.html#af4143048bbe5fef8fd4d54a1e97a5de3", null ],
    [ "OnStoreLoad", "dc/de8/class_static_transmitter.html#ac23b17ac886da8ddb8d18a83589e79f2", null ],
    [ "OnStoreSave", "dc/de8/class_static_transmitter.html#a7abc58eca56947edc62973410ed81e15", null ],
    [ "OnSwitchOn", "dc/de8/class_static_transmitter.html#a80a61eef105a6d8b6d4f41eae20759ed", null ],
    [ "OnWorkStart", "dc/de8/class_static_transmitter.html#a1660bac03386fc53bf3d1ee66890c865", null ],
    [ "OnWorkStop", "dc/de8/class_static_transmitter.html#a1086bedc3746f40892e32db0f1b5e3a8", null ],
    [ "SetActions", "dc/de8/class_static_transmitter.html#a23ad43bbed16135e10bc40ad4eb450d8", null ],
    [ "SetNextFrequency", "dc/de8/class_static_transmitter.html#aacecb48a67fc79b843794219d7730d02", null ]
];