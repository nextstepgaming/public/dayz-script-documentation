var class_hunger_mdfr =
[
    [ "ActivateCondition", "db/dc4/class_hunger_mdfr.html#acabb697bc6172a1ed8fa78cbbe4ad36f", null ],
    [ "DeactivateCondition", "db/dc4/class_hunger_mdfr.html#ada229274b4f7ca7598606a0381a6628b", null ],
    [ "Init", "db/dc4/class_hunger_mdfr.html#a4932507850fda0471f2e931cf4e42747", null ],
    [ "OnReconnect", "db/dc4/class_hunger_mdfr.html#a48ed22ea1591601ca6b20e7dfaea61da", null ],
    [ "OnTick", "db/dc4/class_hunger_mdfr.html#a5d15a66fdb60d234843e45559a231982", null ],
    [ "m_EnergyDelta", "db/dc4/class_hunger_mdfr.html#a126849a75eb92c32beadbcee25ded518", null ],
    [ "m_LastEnergyLevel", "db/dc4/class_hunger_mdfr.html#ac95af57bbdfa6bbb6dcbf54d23cf5a9c", null ],
    [ "m_MovementState", "db/dc4/class_hunger_mdfr.html#ad417e087dcc80e26ec994c18b4ff3a69", null ]
];